export const languages = [
    {
        value: 101,
        label: "Punjabi"
    },
    {
        value: 102,
        label: "English"
    },
    {
        value: 103,
        label: "Hindi"
    },
    {
        value: 104,
        label: "Marathi"
    }
]