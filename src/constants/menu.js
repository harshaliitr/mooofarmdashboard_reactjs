const data = [
  // {
  //   id: "dashboard",
  //   icon: "iconsminds-shop-4",
  //   label: "menu.dashboard",
  //   to: "/app/dashboard"
  // },
  {
    id: "sessions",
    icon: "iconsminds-over-time-2",
    label: "menu.sessions",
    to: "/app/sessions"
  },
   {
    id: "doctors",
    icon: "iconsminds-doctor",
    label: "menu.doctors",
    to: "/app/doctors"
  },
   {
    id: "doctor_mappings",
    icon: "iconsminds-network",
    label: "menu.doctor_mappings",
    to: "/app/doctors-mapping"
  },
  {
    id: "settings",
    icon: "simple-icon-settings",
    label: "menu.setting",
    to: "/app/settings"
  }
];
export default data;
