/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/


module.exports = {
  /* 01.General */
  "general.copyright": "Gogo React © 2018 All Rights Reserved.",


  /* 02.User Login, Logout, Register */
  
  "user.login-title": "Login",
  "user.register": "Register",
  "user.forgot-password": "Forgot Password",
  "user.reset-password": "Reset Password",
  "user.email": "E-mail",
  "user.password": "Password",
  "user.confirm_password":'Confirm Password',
  "user.forgot-password-question": "Forget password?",
  "user.fullname": "Full Name",
  "user.login-button": "LOGIN",
  "user.register-button": "REGISTER",
  "user.reset-password-button":"RESET",
  "user.buy": "BUY",
  "user.username":"Username",
  "user.first_name":"First Name",
  "user.last_name":"Last Name",
  "user.role":"Role",
  "user.mobile_no":"Mobile Number",
  "user.organzation_id":"Organization",
  "user.edit-user":"Update User",
  "user.add-user":"Add New User",
  "user.qualification":"Qualification",
  "user.department":"Department",
  "user.details":"Details",
  "user.blog":"Blog",
  "user.linkedin":"Linkedin",
  "user.job_role":"Job Role",


  /* 03.Menu */
  "menu.app": "Home",
  "menu.dashboard": "Dashboard",
  "menu.users": "Users",
  "menu.gogo": "Gogo",
  "menu.start": "Start",
  "menu.second-menu": "Second Menu",
  "menu.second": "Second",
  "menu.ui": "UI",
  "menu.charts": "Charts",
  "menu.chat": "Chat",
  "menu.survey": "Survey",
  "menu.todo": "Todo",
  "menu.search" :"Search",
  "menu.docs": "Docs",
  "menu.blank-page": "Blank Page",
  "menu.sessions":'Sessions',
   "menu.doctors": "Doctors",
  "menu.doctor_mappings":'Doctor Mappings',
  "menu.setting":'Settings',
  


 /* 04.Error Page */
 "pages.error-title": "Ooops... looks like an error occurred!",
 "pages.error-code": "Error code",
 "pages.go-back-home": "GO BACK HOME",
};
