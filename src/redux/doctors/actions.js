import {
  GET_DOCTORS,
  GET_DOCTORS_SUCCESS,
  GET_DOCTORS_ERROR,
  GET_DOCTORS_MAPPING,
  GET_DOCTORS_MAPPING_SUCCESS,
  GET_DOCTORS_MAPPING_ERROR,
  UPDATE_DOCTORS_MAPPING,
  UPDATE_DOCTORS_MAPPING_SUCCESS,
  UPDATE_DOCTORS_MAPPING_ERROR,
  ADD_UPDATE_DOCTOR,
  ADD_UPDATE_DOCTOR_SUCCESS,
  ADD_UPDATE_DOCTOR_ERROR,
  GET_VILLAGES,
  GET_VILLAGES_SUCCESS,
  GET_COUNTRY,
  GET_COUNTRY_SUCCESS,
  GET_STATES,
  GET_STATES_SUCCESS,
  GET_DISTRICTS,
  GET_DISTRICTS_SUCCESS,
  GET_GENDERS,
  GET_GENDERS_SUCCESS,
  DELETE_DOCTOR,
  DELETE_DOCTOR_SUCCESS,
  DELETE_DOCTOR_ERROR,
  GET_DOCTOR,
  GET_DOCTOR_SUCCESS,
  GET_DOCTOR_ERROR,
  UPDATE_DOC_STATUS,
  GET_EDUCATION,
  GET_EDUCATION_SUCCESS
} from "../actions";

export const getDoctors = (page, pageSize, history) => ({
  type: GET_DOCTORS,
  payload: { history },
  page,
  pageSize,
});

export const getDoctorsSuccess = (data) => ({
  type: GET_DOCTORS_SUCCESS,
  payload: data,
});

export const getDoctorsError = (message) => ({
  type: GET_DOCTORS_ERROR,
  payload: message,
});

//mapping
export const getDoctorsMapping = (
  page,
  pageSize,
  district,
  village,
  doctor,
  history
) => ({
  type: GET_DOCTORS_MAPPING,
  payload: { history },
  page,
  pageSize,
  district,
  village,
  doctor,
});

export const getDoctorsMappingSuccess = (data) => ({
  type: GET_DOCTORS_MAPPING_SUCCESS,
  payload: data,
});

export const getDoctorsMappingError = (message) => ({
  type: GET_DOCTORS_MAPPING_ERROR,
  payload: message,
});

export const updateSortOrder = (data, history) => ({
  type: UPDATE_DOCTORS_MAPPING,
  payload: { data, history },
});

export const updateDocStatus = (data, history) => ({
  type: UPDATE_DOC_STATUS,
  payload: { data, history },
});

export const updateDoctorsMappingSuccess = (data) => ({
  type: UPDATE_DOCTORS_MAPPING_SUCCESS,
  payload: data,
});

export const updateDoctorsMappingError = (message) => ({
  type: UPDATE_DOCTORS_MAPPING_ERROR,
  payload: message,
});

export const addupdateDoctor = (data, history) => ({
  type: ADD_UPDATE_DOCTOR,
  payload: { data, history },
});

export const addupdateDoctorSuccess = (data) => ({
  type: ADD_UPDATE_DOCTOR_SUCCESS,
  payload: data,
});

export const addupdateDoctorError = (message) => ({
  type: ADD_UPDATE_DOCTOR_ERROR,
  payload: message,
});

export const getVillages = (data) => ({
  type: GET_VILLAGES,
  payload: data,
});

export const getVillagesSuccess = (data) => ({
  type: GET_VILLAGES_SUCCESS,
  payload: data,
});

export const getCountry = () => ({
  type: GET_COUNTRY,
  payload: {},
});

export const getCountrySuccess = (data) => ({
  type: GET_COUNTRY_SUCCESS,
  payload: data,
});

export const getStates = (data) => ({
  type: GET_STATES,
  payload: data,
});

export const getStatesSuccess = (data) => ({
  type: GET_STATES_SUCCESS,
  payload: data,
});

export const getDistricts = (data) => ({
  type: GET_DISTRICTS,
  payload: data,
});

export const getDistrictsSuccess = (data) => ({
  type: GET_DISTRICTS_SUCCESS,
  payload: data,
});

export const getGenders = () => ({
  type: GET_GENDERS,
  payload: {},
});

export const getGendersSuccess = (data) => ({
  type: GET_GENDERS_SUCCESS,
  payload: data,
});

export const getEducation = () => ({
  type: GET_EDUCATION,
  payload: {},
});

export const getEducationSuccess = (data) => ({
  type: GET_EDUCATION_SUCCESS,
  payload: data,
});

export const deleteDoctor = (id, history) => ({
  type: DELETE_DOCTOR,
  payload: { id, history },
});

export const deleteDoctorSuccess = (data) => ({
  type: DELETE_DOCTOR_SUCCESS,
  payload: data,
});

export const deleteDoctorError = (data) => ({
  type: DELETE_DOCTOR_ERROR,
  payload: data,
});

export const getDoctor = (id) => ({
  type: GET_DOCTOR,
  payload: { id },
});

export const getDoctorSuccess = (data) => ({
  type: GET_DOCTOR_SUCCESS,
  payload: data,
});

export const getDoctorError = (data) => ({
  type: GET_DOCTOR_ERROR,
  payload: data,
});
