import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  GET_DOCTORS,
  GET_DOCTORS_MAPPING,
  UPDATE_DOCTORS_MAPPING,
  ADD_UPDATE_DOCTOR,
  GET_VILLAGES,
  GET_COUNTRY,
  GET_DISTRICTS,
  GET_GENDERS,
  DELETE_DOCTOR,
  GET_DOCTOR,
  UPDATE_DOC_STATUS,
  GET_EDUCATION,
  GET_STATES
} from "../actions";
import {
  getDoctorsSuccess,
  getDoctorsError,
  getDoctorsMappingSuccess,
  getDoctorsMappingError,
  updateDoctorsMappingSuccess,
  updateDoctorsMappingError,
  addupdateDoctorSuccess,
  addupdateDoctorError,
  getVillagesSuccess,
  getCountrySuccess,
  getDistrictsSuccess,
  getStatesSuccess,
  getGendersSuccess,
  getEducationSuccess,
  deleteDoctorSuccess,
  deleteDoctorError,
  getDoctorSuccess,
  getDoctorError,
} from "./actions";
import axios from "axios";
const dotnetApiUrl = "http://52.148.95.218:46373/api";
const apiUrl =
  process.env.REACT_APP_API_URL || "http://api.mooofarm.com/admin";
// "http://localhost:4000/admin";
let token = localStorage.getItem("token");
const headers = {
  "Content-Type": "application/json",
  Authorization: token,
};

const addToken = () => {
  let token = localStorage.getItem("token");
  headers.Authorization = token;
};

// get doctors
function* getDoctorsApi({ payload, page, pageSize }) {
  try {
    // addToken();
    let params = { page, pageSize };
    const session = yield call(getApiAsync, params, "getDoctors");
    if (session.data.meta.status) {
      yield put(getDoctorsSuccess(session.data));
    } else {
      yield put(getDoctorsError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

// get mapping
function* getMappingApi({
  payload,
  page,
  pageSize,
  district,
  village,
  doctor,
}) {
  try {
    // addToken();
    let params = { page, pageSize, district, village, doctor };
    const session = yield call(getApiAsync, params, "getDoctorMappings");
    if (session.data.meta.status) {
      yield put(getDoctorsMappingSuccess(session.data));
    } else {
      yield put(getDoctorsMappingError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getDoctorsMappingError(error.message));
  }
}

function* updateMappingApi({ payload }) {
  try {
    // addToken();
    let body = payload.data;
    const session = yield call(postApiAsync, body, "mapping/update");
    if (session.data.meta.status) {
      yield put(updateDoctorsMappingSuccess(session.data));
    } else {
      yield put(updateDoctorsMappingError(session.data.meta.message));
    }
  } catch (error) {
    yield put(updateDoctorsMappingError(error.message));
  }
}

function* addUpdateDoctorApi({ payload }) {
  try {
    let body = payload.data;
    const { history } = payload;
    const session = yield call(createDoctorApi, body, "");
    if (session.data.Status === 1) {
      yield put(addupdateDoctorSuccess(session.data));
      history.push("/");
      history.replace("/app/doctors");
    } else {
      yield put(addupdateDoctorError(session.data.Message));
    }
  } catch (error) {
    yield put(addupdateDoctorError(error.message));
  }
}

function* getContryApi({ payload }) {
  try {
    const session = yield call(getApiAsync, '', "getCountry");
    if (session.data.meta.status) {
      yield put(getCountrySuccess(session.data));
    } else {
      yield put(getDoctorsError(session.data.Message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

function* getVillagesApi({ payload }) {
  try {
    const session = yield call(getDotnetApiAsync, payload, `Village/Return`);
    if (session.data.Status === 1)  {
      yield put(getVillagesSuccess(session.data.DetailObject));
    } else {
      yield put(getDoctorsError(session.data.Message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

function* getDistrictsApi({ payload }) {
  try {
    const session = yield call(getDotnetApiAsync, payload, "District/Return");
    if (session.data.Status === 1)  {
      yield put(getDistrictsSuccess(session.data.DetailObject));
    } else {
      yield put(getDoctorsError(session.data.Message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

function* getStatesApi({ payload }) {
  try {
    const session = yield call(getDotnetApiAsync, payload, "State/Return");
    if (session.data.Status === 1)  {
      yield put(getStatesSuccess(session.data.DetailObject));
    } else {
      yield put(getDoctorsError(session.data.Message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

function* getGendersApi({ payload }) {
  try {
    const session = yield call(getApiAsync, '', "genders");
    if (session.data.meta.status) {
      yield put(getGendersSuccess(session.data));
    } else {
      yield put(getDoctorsError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

function* getEducationApi({ payload }) {
  try {
    const session = yield call(newGetEducationApi, '', '');
    if (session.data.Status === 1) {
      yield put(getEducationSuccess(session.data));
    } else {
      yield put(getDoctorsError(session.data.Message));
    }
  } catch (error) {
    yield put(getDoctorsError(error.message));
  }
}

function* deleteDoctorApi({ payload }) {
  try {
    let id = payload.id;
    const {history} = payload;
    const session = yield call(postApiAsync, '', `doctors/${id}`);
    if (session.data.meta.status) {
      yield put(deleteDoctorSuccess(session.data));
      history.push("/");
      history.replace("/app/doctors");
    } else {
      yield put(deleteDoctorError(session.data.meta.message));
    }
  } catch (error) {
    yield put(deleteDoctorError(error.message));
  }
}


function* getDoctorApi({ payload }) {
  try {
    let id = payload.id;
    const session = yield call(getApiAsync, `doctors/${id}`);
    if (session.data.meta.status) {
      yield put(getDoctorSuccess(session.data));
    } else {
      yield put(getDoctorError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getDoctorError(error.message));
  }
}

function* updateDocStatusApi({ payload }) {
  try {
    const session = yield call(postApiAsync, payload.data, `doctors/updateStatus`);
    if (session.data.meta.status) {
      yield put(addupdateDoctorSuccess(session.data));
    } else {
      yield put(addupdateDoctorError(session.data.meta.message));
    }
  } catch (error) {
    yield put(addupdateDoctorError(error.message));
  }
}



const getApiAsync = async (data, url) =>
  await axios
    .get(`${apiUrl}/${url}`, { params: data }, { headers })
    .then((data) => data)
    .catch((error) => error);

const postApiAsync = async (data, url) =>
  await axios
    .post(`${apiUrl}/${url}`, data, { headers })
    .then((data) => data)
    .catch((error) => error);

const getDotnetApiAsync = async (data, url) =>
  await axios
    .get(`${dotnetApiUrl}/${url}`, { params: {...data} }, { headers })
    .then((data) => data)
    .catch((error) => error);

const newGetEducationApi = async (data, url) =>
  await axios
    .get(`${dotnetApiUrl}/UserDetail/EducationReturn?EduID=-1`, { ...data }, { headers })
    .then((data) => data)
    .catch((error) => error);

const createDoctorApi = async (data, url) =>
  await axios
    .post(`${dotnetApiUrl}/UserDetail/DoctorCreate`, { ...data }, { headers })
    .then((data) => data)
    .catch((error) => error);

// action wacthers

export function* watchGetDoctors() {
  yield takeEvery(GET_DOCTORS, getDoctorsApi);
}

export function* watchGetMapping() {
  yield takeEvery(GET_DOCTORS_MAPPING, getMappingApi);
}

export function* watchUpdateMapping() {
  yield takeEvery(UPDATE_DOCTORS_MAPPING, updateMappingApi);
}

export function* watchAddUpdateDoctor() {
  yield takeEvery(ADD_UPDATE_DOCTOR, addUpdateDoctorApi);
}

export function* watchGetCountry() {
  yield takeEvery(GET_COUNTRY, getContryApi);
}

export function* watchGetVillages() {
  yield takeEvery(GET_VILLAGES, getVillagesApi);
}

export function* watchGetDistricts() {
  yield takeEvery(GET_DISTRICTS, getDistrictsApi);
}

export function* watchGetStates() {
  yield takeEvery(GET_STATES, getStatesApi);
}

export function* watchGetGenders() {
  yield takeEvery(GET_GENDERS, getGendersApi);
}

export function* watchGetEducation() {
  yield takeEvery(GET_EDUCATION, getEducationApi);
}

export function* watchDeleteDoctor() {
  yield takeEvery(DELETE_DOCTOR, deleteDoctorApi);
}

export function* watchGetDoctor() {
  yield takeEvery(GET_DOCTOR, getDoctorApi);
}

export function* watchUpdateDoctorStatus() {
  yield takeEvery(UPDATE_DOC_STATUS, updateDocStatusApi);
}

export default function* rootSaga() {
  yield all([
    fork(watchGetDoctors),
    fork(watchGetMapping),
    fork(watchUpdateMapping),
    fork(watchAddUpdateDoctor),
    fork(watchGetCountry),
    fork(watchGetVillages),
    fork(watchGetDistricts),
    fork(watchGetStates),
    fork(watchGetGenders),
    fork(watchGetEducation),
    fork(watchDeleteDoctor),
    fork(watchGetDoctor),
    fork(watchUpdateDoctorStatus)
  ]);
}
