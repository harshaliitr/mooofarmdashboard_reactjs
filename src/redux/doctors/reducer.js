import {
  GET_DOCTORS,
  GET_DOCTORS_SUCCESS,
  GET_DOCTORS_ERROR,
  GET_DOCTORS_MAPPING,
  GET_DOCTORS_MAPPING_SUCCESS,
  GET_DOCTORS_MAPPING_ERROR,
  UPDATE_DOCTORS_MAPPING,
  UPDATE_DOCTORS_MAPPING_SUCCESS,
  UPDATE_DOCTORS_MAPPING_ERROR,
  ADD_UPDATE_DOCTOR,
  ADD_UPDATE_DOCTOR_SUCCESS,
  ADD_UPDATE_DOCTOR_ERROR,
  GET_VILLAGES,
  GET_VILLAGES_SUCCESS,
  GET_COUNTRY,
  GET_COUNTRY_SUCCESS,
  GET_STATES,
  GET_STATES_SUCCESS,
  GET_DISTRICTS,
  GET_DISTRICTS_SUCCESS,
  GET_GENDERS,
  GET_GENDERS_SUCCESS,
  GET_EDUCATION,
  GET_EDUCATION_SUCCESS,
  DELETE_DOCTOR,
  DELETE_DOCTOR_SUCCESS,
  DELETE_DOCTOR_ERROR,
  GET_DOCTOR,
  GET_DOCTOR_SUCCESS,
  GET_DOCTOR_ERROR,
  UPDATE_DOC_STATUS
} from "../actions";

const INIT_STATE = {
  loading: false,
  doctors_data: {},
  mapping_data: {},
  doctor: {},
  villages: [],
  districts: [],
  states: [],
  genders: [],
  educationList: [],
  countries: [],
  message: "",
  error: "",
  success: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_DOCTORS:
      return { ...state, loading: true, error: "" };
    case GET_DOCTORS_SUCCESS:
      return {
        ...state,
        doctors_data: action.payload.data,
        loading: false,
        error: "",
      };
    case GET_DOCTORS_ERROR:
      return {
        ...state,
        success: false,
        loading: false,
        error: action.payload,
      };
    case GET_DOCTORS_MAPPING:
      return { ...state, loading: true, error: "" };
    case GET_DOCTORS_MAPPING_SUCCESS:
      return {
        ...state,
        mapping_data: action.payload.data,
        loading: false,
        error: "",
      };
    case GET_DOCTORS_MAPPING_ERROR:
      return {
        ...state,
        success: false,
        loading: false,
        error: action.payload,
      };
    case UPDATE_DOCTORS_MAPPING:
      return { ...state, loading: true, success: false, error: "" };
    case UPDATE_DOCTORS_MAPPING_SUCCESS:
      return { ...state, loading: false, success: true, error: "" };
    case UPDATE_DOCTORS_MAPPING_SUCCESS:
      return {
        ...state,
        success: false,
        loading: false,
        error: action.payload,
      };
    case ADD_UPDATE_DOCTOR:
      return { ...state, loading: true, success: false, error: "" };
    case ADD_UPDATE_DOCTOR_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: action.payload,
      };
    case ADD_UPDATE_DOCTOR_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case UPDATE_DOC_STATUS:
      return {
        ...state,
        loading: true,
      };
    case DELETE_DOCTOR:
      return { ...state, loading: true, success: false, error: "" };
    case DELETE_DOCTOR_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case DELETE_DOCTOR_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        message: action.payload,
        error: action.payload,
      };
    case GET_VILLAGES:
      return { ...state, loading: true, success: false, error: "" };
    case GET_VILLAGES_SUCCESS:
      let villageData = action.payload.map(v => {
        return {
          villageID: v.Detail.VillageID,
          villageName: v.Detail.VillageName
        };
      })
      return {
        ...state,
        loading: false,
        success: true,
        villages: villageData,
        error: "",
      };

    case GET_COUNTRY:
      return { ...state, loading: true, success: false, error: "" };
    case GET_COUNTRY_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        countries: action.payload.data,
        error: "",
      };

    case GET_STATES:
      return { ...state, loading: true, success: false, error: "" };
    case GET_STATES_SUCCESS:
      let stateData = action.payload.map(s => {
        return {
          stateID: s.Detail.StateID,
          stateName: s.Detail.StateName
        };
      })
      return {
        ...state,
        loading: false,
        success: true,
        states: stateData,
        error: "",
      };

    case GET_DISTRICTS:
      return { ...state, loading: true, success: false, error: "" };
    case GET_DISTRICTS_SUCCESS:
      let districtData = action.payload.map(d => {
        return {
          districtID: d.Detail.DistrictID,
          districtName: d.Detail.DistrictName
        };
      })
      return {
        ...state,
        loading: false,
        success: true,
        districts: districtData,
        error: "",
      };

    case GET_GENDERS:
      return { ...state, loading: true, success: false, error: "" };
    case GET_GENDERS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        genders: action.payload.data,
        error: "",
      };
    case GET_EDUCATION:
      return { ...state, loading: true, success: false, error: "" };
    case GET_EDUCATION_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        educationList: action.payload.Detail,
        error: "",
      };

    case GET_DOCTOR:
      return { ...state, loading: true, success: false, error: "" };
    case GET_DOCTOR_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        doctor: action.payload,
        error: "",
      };
    case GET_DOCTOR_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: action.payload,
      };
    default:
      return { ...state };
  }
};
