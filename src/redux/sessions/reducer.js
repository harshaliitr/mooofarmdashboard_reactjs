import {
  GET_SESSIONS,
  GET_SESSIONS_SUCCESS,
  GET_SESSIONS_ERROR
  } from "../actions";
  
  const INIT_STATE = {
    loading: false,
    session_data:{
      sessions: [],
      totalCount: 0,
      maxPage: 0,
      currentPage: 0
    },
    message:"",
    error: "",
    success: false
  };
  
  export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case GET_SESSIONS:
        return { ...state, loading: true, error: "" };
      case GET_SESSIONS_SUCCESS:
        return {
          ...state,
          session_data:action.payload.data,
          loading: false,
          error: ""
        };
      case GET_SESSIONS_ERROR:
        return { ...state, success: false, loading: false, error:action.payload };
      default:
        return { ...state };
    }
  };
  