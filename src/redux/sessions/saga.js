import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  GET_SESSIONS
} from "../actions";
import {
  getSessionsSuccess,
  getSessionsError
} from "./actions";
import axios from "axios";
const apiUrl =
  process.env.REACT_APP_API_URL ||  "http://api.mooofarm.com/admin";
// "http://localhost:4000/admin";
let token = localStorage.getItem("token");
const headers = {
  "Content-Type": "application/json",
  Authorization: token
};

const addToken = () => {
  let token = localStorage.getItem("token");
  headers.Authorization = token;
};

// get brands
function* getSessionApi({ payload }) {
  try {
    // addToken();
    const session = yield call(getSessionsAsync, payload.data);
    if (session.data.meta.status) {
      yield put(getSessionsSuccess(session.data));
    } else {
      yield put(getSessionsError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getSessionsError(error.message));
  }
}

const getSessionsAsync = async (data) =>
  await axios
    .get(`${apiUrl}/getSessions`, { params:data }, { headers })
    .then(data => data)
    .catch(error => error);

// action wacthers

export function* watchGetSessions() {
  yield takeEvery(GET_SESSIONS, getSessionApi);
}


export default function* rootSaga() {
  yield all([
    fork(watchGetSessions)
  ]);
}
