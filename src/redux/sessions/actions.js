import {
  GET_SESSIONS,
  GET_SESSIONS_SUCCESS,
  GET_SESSIONS_ERROR,
} from "../actions";

export const getSessions = (data, history) => ({
  type: GET_SESSIONS,
  payload: { history, data },
});


export const getSessionsSuccess = data => ({
  type: GET_SESSIONS_SUCCESS,
  payload: data
});

export const getSessionsError = message => ({
  type: GET_SESSIONS_ERROR,
  payload: message
});
