import { defaultLocale, localeOptions } from "../../constants/defaultValues";

import {
  CHANGE_LOCALE,
  GET_SETINGS,
  GET_SETINGS_SUCCESS,
  GET_SETINGS_ERROR,
  UPDATE_SETINGS,
  UPDATE_SETINGS_SUCCESS,
  DELETE_SETINGS,
  DELETE_SETINGS_SUCCESS,
} from "../actions";

const INIT_STATE = {
  settings: [],
  loading: false,
  success: false,
  error: "",
  locale:
    localStorage.getItem("currentLanguage") &&
    localeOptions.filter(
      (x) => x.id === localStorage.getItem("currentLanguage")
    ).length > 0
      ? localStorage.getItem("currentLanguage")
      : defaultLocale,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_LOCALE:
      return { ...state, locale: action.payload };
    case GET_SETINGS:
      return { ...state, loading: true, error: "" };
    case GET_SETINGS_SUCCESS:
      return {
        ...state,
        settings: action.payload.data,
        loading: false,
        error: "",
      };
    case GET_SETINGS_ERROR:
      return {
        ...state,
        success: false,
        loading: false,
        error: action.payload,
      };
    case UPDATE_SETINGS:
      return { ...state, loading: true, success: false, error: "" };
    case UPDATE_SETINGS_SUCCESS:
      return { ...state, loading: false, success: true, error: "" };
    case DELETE_SETINGS:
      return { ...state, loading: true, success: false, error: "" };

    case DELETE_SETINGS_SUCCESS:
      return {
        ...state,
        success: false,
        loading: false,
        error: action.payload,
      };

    default:
      return { ...state };
  }
};
