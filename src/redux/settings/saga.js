import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { GET_SETINGS, UPDATE_SETINGS, DELETE_SETINGS } from "../actions";
import {
  getSettingsSuccess,
  getSettingsError,
  updateSettingsSuccess,
  deleteSettingsSuccess,
} from "./actions";
import axios from "axios";
const apiUrl = process.env.REACT_APP_API_URL || "http://api.mooofarm.com/admin";
//  "http://localhost:4000/admin"; 

let token = localStorage.getItem("token");
const headers = {
  "Content-Type": "application/json",
  Authorization: token,
};

const addToken = () => {
  let token = localStorage.getItem("token");
  headers.Authorization = token;
};

// get settings
function* getSettingsApi({ payload, page, pageSize }) {
  try {
    // addToken();
    let params = { page, pageSize };
    const session = yield call(getApiAsync, params, "settings");
    if (session.data.meta.status) {
      yield put(getSettingsSuccess(session.data));
    } else {
      yield put(getSettingsError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getSettingsError(error.message));
  }
}

function* updateSettingApi({ payload }) {
  try {
    // addToken();
    let body = payload.data;
    const session = yield call(postApiAsync, body, "settings/update");
    if (session.data.meta.status) {
      yield put(updateSettingsSuccess(session.data));
    } else {
      yield put(getSettingsError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getSettingsError(error.message));
  }
}

function* deleteSettingApi({ payload }) {
  try {
    let id = payload.id;
    const session = yield call(postApiAsync, "", `settings/delete/${id}`);
    if (session.data.meta.status) {
      yield put(deleteSettingsSuccess(session.data));
    } else {
      yield put(getSettingsError(session.data.meta.message));
    }
  } catch (error) {
    yield put(getSettingsError(error.message));
  }
}

const getApiAsync = async (data, url) =>
  await axios
    .get(`${apiUrl}/${url}`, { params: data }, { headers })
    .then((data) => data)
    .catch((error) => error);

const postApiAsync = async (data, url) =>
  await axios
    .post(`${apiUrl}/${url}`, data, { headers })
    .then((data) => data)
    .catch((error) => error);

// action wacthers

export function* watchGetSettings() {
  yield takeEvery(GET_SETINGS, getSettingsApi);
}

export function* watchDeleteSettings() {
  yield takeEvery(DELETE_SETINGS, deleteSettingApi);
}

export function* watchUpdateSettings() {
  yield takeEvery(UPDATE_SETINGS, updateSettingApi);
}

export default function* rootSaga() {
  yield all([
    fork(watchGetSettings),
    fork(watchDeleteSettings),
    fork(watchUpdateSettings),
  ]);
}
