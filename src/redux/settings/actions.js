import {
  CHANGE_LOCALE,
  GET_SETINGS,
  GET_SETINGS_SUCCESS,
  GET_SETINGS_ERROR,
  UPDATE_SETINGS,
  UPDATE_SETINGS_SUCCESS,
  DELETE_SETINGS,
  DELETE_SETINGS_SUCCESS,
} from "../actions";

export const changeLocale = (locale) => {
  localStorage.setItem("currentLanguage", locale);
  return {
    type: CHANGE_LOCALE,
    payload: locale,
  };
};

export const getSettings = (page, pageSize, history) => ({
  type: GET_SETINGS,
  payload: { history },
  page,
  pageSize,
});

export const getSettingsSuccess = (data) => ({
  type: GET_SETINGS_SUCCESS,
  payload: data,
});

export const getSettingsError = (message) => ({
  type: GET_SETINGS_ERROR,
  payload: message,
});

export const updateSettings = (data, history) => ({
  type: UPDATE_SETINGS,
  payload: { data, history }
});

export const updateSettingsSuccess = (data) => ({
  type: UPDATE_SETINGS_SUCCESS,
  payload: data,
});

export const deleteSettings = (id, history) => ({
  type: DELETE_SETINGS,
  payload: { id, history },
});

export const deleteSettingsSuccess = (data) => ({
  type: DELETE_SETINGS_SUCCESS,
  payload: data,
});
