import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import sessionSagas from './sessions/saga';
import doctorsSagas from './doctors/saga';
import settingSagas from './settings/saga';

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    sessionSagas(),
    doctorsSagas(),
    settingSagas()
  ]);
}
