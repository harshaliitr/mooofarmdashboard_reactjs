import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { auth } from "../../helpers/Firebase";
import { LOGIN_USER, LOGOUT_USER } from "../actions";

import {
  loginUserSuccess,
  loginUserError
} from "./actions";
import axios from "axios";
const apiUrl = "http://52.148.95.218:46373/api";

let token = localStorage.getItem("token");

let appid = "957852";


const headers = {
  "Content-Type": "application/json",
  'appId': appid ,
};

const addToken = () => {
  let token = localStorage.getItem("token");
  headers.Authorization = token;
};




const loginWithEmailPasswordAsync = async (contact, password) =>
  await axios
    .get(`${apiUrl}/UserAuthentication/UserLoginContact/?contact=${contact}&Password=${password}&languageid=102`,{ headers})
    .then(authUser => authUser)
    .catch(error => error); 

function* loginWithEmailPassword({ payload }) {
  const { contact, password } = payload.user;
  const { history } = payload;
  try {
    const loginUser = yield call(loginWithEmailPasswordAsync, contact, password);
    if(loginUser.data.Status === 1) {
      localStorage.setItem("user_id", loginUser.data.UserID);
      localStorage.setItem("token", loginUser.data.Token);
      localStorage.setItem("username", loginUser.data.Detail.UserTypeName);
      localStorage.setItem("userData", JSON.stringify(loginUser.data));
      history.push("/");
      yield put(loginUserSuccess(loginUser.data));
    } else {
      yield put(loginUserError(loginUser.data));
    }
  } catch (error) {
    yield put(loginUserError(error));
  }
}

const logoutAsync = async history => {
  localStorage.removeItem("user_id");
  localStorage.removeItem("username");
  localStorage.removeItem("token");
  localStorage.removeItem('userData');
  localStorage.removeItem('user');
  history.push("/admin/login");
};

function* logout({ payload }) {
  const { history } = payload;
  try {
    yield call(logoutAsync, history);
  } catch (error) {}
}

export function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}

export function* watchLogoutUser() {
  yield takeEvery(LOGOUT_USER, logout);
}



export default function* rootSaga() {
  yield all([
    fork(watchLoginUser),
    fork(watchLogoutUser)
  ]);
}



