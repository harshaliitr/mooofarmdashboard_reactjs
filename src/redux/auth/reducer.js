import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGOUT_USER,
} from "../actions";

const INIT_STATE = {
    user: localStorage.getItem('user_id') || "",
    loading: false,
    users:[],
    userData:{},
    error: ''
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return { ...state, loading: true, error: '' };
        case LOGIN_USER_SUCCESS:
            return { ...state, loading: false, userData: action.payload, error: '', user: action.payload.UserID };
        case LOGIN_USER_ERROR:
            return { ...state, loading: false, user: '', error: action.payload.Message };
        case LOGOUT_USER:
        default: return { ...state };
    }
}
