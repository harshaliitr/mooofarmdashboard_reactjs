import { combineReducers } from "redux";
import settings from "./settings/reducer";
import menu from "./menu/reducer";
import authUser from "./auth/reducer";
import doctors from './doctors/reducer';
import sessions from "./sessions/reducer";

const reducers = combineReducers({
  menu,
  settings,
  authUser,
  sessions,
  doctors
});

export default reducers;
