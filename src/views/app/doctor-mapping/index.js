import React from "react";
import { getDoctorsMapping, updateSortOrder } from "../../../redux/actions";
import { connect } from "react-redux";
import Table from "react-table";
import { Button, Modal, ModalHeader, ModalBody, Label, FormGroup } from "reactstrap";
import { Formik, Form, Field } from "formik";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class DcotorMapping extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          Header: "Doctor",
          accessor: "FullName"
        },
        // {
        //   Header: "Village",
        //   accessor: "VillageName"
        // },
        // {
        //   Header: "District",
        //   accessor: "DistrictName",
        // },
        {
          Header: "Sort Order",
          // sortable: false,
          filterable: false,
          accessor: "SortOrder"
        },
        {
          Header: "Actions",
          sortable: false,
          filterable: false,
          Cell: record => (
            <span>
              <Button
                className=""
                color="primary"
                onClick={() => this.handleEditModel(record)}
              >
                <div title="edit" className="simple-icon-pencil"></div>
              </Button>
            </span>
          )
        }
      ],
      page: 0,
      pageSize: 50,
      maxPage: 0,
      total: 0,
      selected: {},
      isEdit: false,
      mapping: [],
      district: "",
      village: "",
      doctor: "",
      sortOrder: ''
    };
  }


  handleCancel = () => {
    this.setState({ isEdit: false });
  };

  handleEditModel = (record) => {
    // console.log(JSON.stringify(record));
    this.setState({
      selected: record.original,
      sortOrder: record.original.SortOrder,
      isEdit: !this.state.isEdit,
    });
  }


  addSupport = values => {
    if (
      values.sortOrder !== ""
    ) {
      let vals = {
        doctor_id: this.state.selected.UserID,
        sort_order: values.sortOrder
      };
      this.props.updateSortOrder(vals, this.props.history);
    }
  };

  componentDidUpdate(preProps) {
    if (this.props.mapping_data !== preProps.mapping_data) {
      this.setState({
        total: this.props.mapping_data.totalCount,
        maxPage: this.props.mapping_data.maxPage,
        page: this.props.mapping_data.currentPage,
        mapping: this.props.mapping_data.mapping
      })
    }

    if (preProps.success !== this.props.success) {
      let { page, pageSize } = this.state;
      this.handleCancel();
      this.props.getDoctorsMapping(
        page === 0 ? 1 : page,
        pageSize,
        "",
        "",
        "",
        this.props.history
      );
    }
  }

  filterCaseInsensitive(filter, row) {
    const id = filter.pivotId || filter.id;
    return (
      row[id] !== undefined ?
        String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
        :
        true
    );
  }


  fetchData = (data) => {
    let { district, village, doctor } = this.state;
    if (data.filtered.length) {
      for (let s of data.filtered) {
        if (s.id.includes("district")) {
          district = s.value.trim();
        }
        if (s.id.includes("village")) {
          village = s.value.trim();

        }
        if (s.id.includes("doctor")) {
          doctor = s.value.trim();
        }
      }
      this.props.getDoctorsMapping(
        data.page === 0 ? 1 : data.page,
        data.pageSize,
        district,
        village,
        doctor,
        this.props.history
      );
    } else {
      this.props.getDoctorsMapping(
        data.page + 1,
        data.pageSize,
        district,
        village,
        doctor,
        this.props.history
      );
    }
  }

  render() {
    const { columns, pageSize, maxPage, mapping, sortOrder, isEdit } = this.state;

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
              <h1>Doctor's Mapping </h1>
              <div className="text-zero top-right-button-container">
                <ExcelFile filename="doctors-mapping" element={(<Button > Download as excel </Button>)}>
                  <ExcelSheet data={mapping} name="mapping">
                    <ExcelColumn label="Doctor Name" value="FullName" />
                    {/* <ExcelColumn label="Village" value="VillageName" />
                    <ExcelColumn label="District" value="DistrictName" /> */}
                    <ExcelColumn label="Sort Order" value="SortOrder" />
                  </ExcelSheet>
                </ExcelFile>
              </div>
            </div>
          </div>
          <div className="separator mb-5"></div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
              {/* {mapping.length || this.props.loading === false ? ( */}
              <Table
                manual
                // filterable
                showPageSizeOptions={false}
                onFetchData={state => this.fetchData(state)}
                pages={maxPage}
                defaultFilterMethod={(filter, row) => this.filterCaseInsensitive(filter, row)}
                columns={columns}
                data={mapping}
                defaultPageSize={pageSize}
                className="-striped -highlight"
                loading={this.props.loading}
              />
              {/* ) : (
                <div className="loading" />
              )} */}
            </div>
          </div>
        </div>

        <Modal isOpen={isEdit} toggle={this.handleCancel}>
          <ModalHeader toggle={this.handleCancel}>Update Sort order</ModalHeader>
          <ModalBody>
            <Formik initialValues={this.state} onSubmit={this.addSupport}>
              {({ errors, handleChange, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>Sort Order</Label>
                    <Field
                      className="form-control"
                      name="sortOrder"
                      required
                      onChange={handleChange("sortOrder")}
                    />
                    {errors.sortOrder && touched.sortOrder && (
                      <div className="invalid-feedback d-block">
                        {errors.sortOrder}
                      </div>
                    )}
                  </FormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <Button
                      color="primary"
                      type="submit"
                      className={`btn-shadow btn-multiple-state ${
                        this.props.loading ? "show-spinner" : ""
                        }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">Update</span>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </ModalBody>
        </Modal>

      </React.Fragment>
    );
  }
}
const mapStateToProps = props => {
  const { success, mapping_data, message, loading, error } = props.doctors;
  return { success, mapping_data, message, loading, error };
};

export default connect(mapStateToProps, {
  getDoctorsMapping,
  updateSortOrder
})(DcotorMapping);
