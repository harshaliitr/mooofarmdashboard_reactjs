import React from "react";
import {
  getDoctor,
  addupdateDoctor,
  getVillages,
  getCountry,
  getDistricts,
  getStates,
  getGenders,
  getEducation
} from "../../../redux/actions";
import { languages } from "./../../../constants/constants"
import { connect } from "react-redux";
import { Formik, Form, Field } from "formik";
import { Label, FormGroup, Button, Input } from "reactstrap";
import makeAnimated from "react-select/animated"
import Select from 'react-select';

class AddUpdateDoctor extends React.Component {
  constructor(props) {
    super(props);
    const { doctor } = props;
    this.state = {
      isEdit: doctor ? true : false,
      fullName: doctor ? doctor.FullName : "",
      age: doctor ? doctor.Age : "",
      emailID: doctor ? doctor.PassMail : "",
      contactNo: doctor ? doctor.ContactNo : "",
      villageID: doctor ? doctor.VillageID : "",
      districtID: doctor ? doctor.DistrictID : "",
      displayPic: doctor ? doctor.DisplayPic : "",
      genderID: doctor ? doctor.GenderID : "",
      experience: doctor ? doctor.Experience : "",
      availableFrom: doctor ? doctor.AvailableFrom : "",
      availableTo: doctor ? doctor.AvailableTo : "",
      selectedLanguages: "",
      selectedStateID: ""
      // father_name: doctor ? doctor.FatherName : "",
      // address: doctor ? doctor.AddressLine : "",
      // is_locked: doctor ? doctor.isLocked : false,
      // is_active: doctor ? doctor.isActive : true,
      // is_deleted: doctor ? doctor.isDeleted : false,
      // status: doctor ? doctor.status : false,
    };
    this.addDoctor = this.addDoctor.bind(this);
  }

  componentDidMount() {
    const { district_id, selectedStateID } = this.state;

    if (district_id) {
      this.props.getVillages(district_id);
    }
    if (selectedStateID) {
      this.props.getDistricts({ languageid: 102, countryid: -1, stateid: selectedStateID, districtid: -1 });
    }
    this.props.getStates({ languageid: 102, countryid: -1, stateid: -1 });

    this.props.getGenders();

    this.props.getEducation();
  }

  handleGenderChange = (data) => {
    this.setState({
      genderID: data,
    });
  };

  handleDistrictChange = (data) => {
    this.setState({
      districtID: data.districtID,
    });
    this.props.getVillages({ languageid: 102, countryid: -1, stateid: this.state.selectedStateID, districtid: data.districtID, villageid: -1 });
  };

  handleVillageChange = (data) => {
    this.setState({
      villageID: data.villageID,
    });
  };

  // handleSatusChange = (data) => {
  //   this.setState({
  //     status: !this.state.status
  //   });
  // }

  handleEducationChange = (data) => {
    this.setState({
      educationID: data,
    });
  };

  handleStateChange = (data) => {
    console.log("data", data)
    this.setState({
      selectedStateID: data.stateID,
    });
    this.props.getDistricts({ languageid: 102, countryid: -1, stateid: data.stateID, districtid: -1 });
  };

  handleLanguageChange = (e) => {
    let ll = e ? e.map(l => l.value) : []
    this.setState({
      selectedLanguages: ll.join(", ")
    })
  }

  addDoctor(values) {
    const { doctor } = this.props;
    let doctor_id = doctor ? doctor.UserID : "";
    const { status, villageID, district_id, genderID, educationID, selectedLanguages } = this.state;
    if (!this.props.loading) {
      if (values.emailID !== "" && values.fullName !== "" && values.contactNo !== "") {
        let vals = {
          UserTypeID: "9261",
          FullName: values.fullName,
          GenderID: String(genderID),
          Age: values.age,
          DisplayPic: "Empty.png",
          ContactNo: values.contactNo,
          EmailID: values.emailID,
          VillageID: String(villageID),
          EducationID: educationID,
          Experience: values.experience,
          AvailableFrom: values.availableFrom,
          AvailableTo: values.availableTo,
          Description: values.doctorDescription,
          LanguageString: selectedLanguages,
          Passkey: "Mooo123"
          // father_name: values.father_name,
          // address: values.address,
          // district_id: String(district_id),
          // is_locked: 0,
          // is_active: 1,
          // is_deleted: 0,
          // status: status
        };
        this.props.addupdateDoctor(vals, this.props.history);
      }
    }
  };

  render() {
    const { districts, villages, genders, countries, educationList, states } = this.props;
    const { isEdit } = this.state;
    const animatedComponents = makeAnimated();

    console.log(states)

    return (
      <React.Fragment>
        <Formik initialValues={this.state} onSubmit={this.addDoctor}>
          {({ errors, handleChange, touched }) => (
            <Form className="av-tooltip tooltip-label-bottom">
              {/* Full Name */}
              <FormGroup className="form-group has-float-label">
                <Label>Full Name</Label>
                <Field
                  className="form-control"
                  name="fullName"
                  required
                  onChange={handleChange("fullName")}
                />
                {errors.fullName && touched.fullName && (
                  <div className="invalid-feedback d-block">
                    {errors.fullName}
                  </div>
                )}
              </FormGroup>

              {/* Age */}
              <FormGroup className="form-group has-float-label">
                <Label>Age</Label>
                <Field
                  className="form-control"
                  name="age"
                  required
                  onChange={handleChange("age")}
                />
                {errors.age && touched.age && (
                  <div className="invalid-feedback d-block">{errors.age}</div>
                )}
              </FormGroup>

              {/* Gender */}
              <FormGroup className="form-group has-float-label">
                <Label>Select Gender</Label>
                <Input
                  className="form-control"
                  required
                  value={this.state.genderID}
                  onChange={(e) =>
                    this.handleGenderChange(e.currentTarget.value)
                  }
                  type="select"
                  name="genderID"
                  id="genderID"
                >
                  <option value="">Select Gender</option>
                  {genders.length &&
                    genders.map((gender, i) => {
                      return (
                        <option key={i + 2} value={gender.GenderID}>
                          {gender.GenderName}
                        </option>
                      );
                    })}
                </Input>

                {errors.genderID && touched.genderID && (
                  <div className="invalid-feedback d-block">
                    {errors.genderID}
                  </div>
                )}
              </FormGroup>

              {/* Language */}
              <FormGroup className="form-group has-float-label">
                <Label className="language-float-label">Select Languages known</Label>
                <Select
                  closeMenuOnSelect={false}
                  components={animatedComponents}
                  defaultValue={[languages[0], languages[1]]}
                  isMulti
                  options={languages}
                  onChange={(e) => this.handleLanguageChange(e)}
                  className="language-select-component"
                />
              </FormGroup>

              {/* Email */}
              <FormGroup className="form-group has-float-label">
                <Label>Email</Label>
                <Field
                  className="form-control"
                  name="emailID"
                  type="email"
                  required
                  onChange={handleChange("emailID")}
                />
                {errors.emailID && touched.emailID && (
                  <div className="invalid-feedback d-block">
                    {errors.emailID}
                  </div>
                )}
              </FormGroup>

              {/* Contact */}
              <FormGroup className="form-group has-float-label">
                <Label>Contact Number</Label>
                <Field
                  className="form-control"
                  name="contactNo"
                  maxLength="10"
                  minLength="10"
                  onChange={handleChange("contactNo")}
                />
                {errors.contactNo && touched.contactNo && (
                  <div className="invalid-feedback d-block">
                    {errors.contactNo}
                  </div>
                )}
              </FormGroup>

              {/* State */}
              <FormGroup className="form-group has-float-label">
                <Label className="state-float-label">State</Label>
                <Select
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={states}
                  onChange={(e) => this.handleStateChange(e)}
                  getOptionLabel={option => option["stateName"]}
                  getOptionValue={option => option["stateID"]}
                  className="state-select-component"
                />
              </FormGroup>

              {/* District */}
              <FormGroup className="form-group has-float-label">
                <Label className="district-float-label">District</Label>
                <Select
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={districts}
                  onChange={(e) => this.handleDistrictChange(e)}
                  getOptionLabel={option => option["districtName"]}
                  getOptionValue={option => option["districtID"]}
                  className="district-select-component"
                />
              </FormGroup>

              {/* Village */}
              <FormGroup className="form-group has-float-label">
                <Label className="village-float-label">Village</Label>
                <Select
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={villages}
                  onChange={(e) => this.handleVillageChange(e)}
                  getOptionLabel={option => option["villageName"]}
                  getOptionValue={option => option["villageID"]}
                  className="village-select-component"
                />
              </FormGroup>

              {/* Education */}
              <FormGroup className="form-group has-float-label">
                <Label>Education</Label>
                <Input
                  className="form-control"
                  required
                  value={this.state.educationID}
                  onChange={(e) =>
                    this.handleEducationChange(e.currentTarget.value)
                  }
                  type="select"
                  name="educationID"
                >
                  <option value="">Select Education</option>
                  {educationList.length &&
                    educationList.map((edu, i) => {
                      return (
                        <option key={i + 3} value={edu.EducationID}>
                          {edu.EducationName}
                        </option>
                      );
                    })}
                </Input>
                {errors.educationID && touched.educationID && (
                  <div className="invalid-feedback d-block">
                    {errors.educationID}
                  </div>
                )}
              </FormGroup>

              <FormGroup className="form-group has-float-label">
                <Label>Experience (in years)</Label>
                <Field
                  className="form-control"
                  name="experience"
                  required
                  onChange={handleChange("experience")}
                />
                {errors.experience && touched.experience && (
                  <div className="invalid-feedback d-block">
                    {errors.experience}
                  </div>
                )}
              </FormGroup>

              <FormGroup className="form-group has-float-label">
                <Label>Available From</Label>
                <Input
                  className="form-control"
                  name="availableFrom"
                  required
                  type="time"
                  onChange={handleChange("availableFrom")}
                />
                {/* <TimePicker
                  onChange={(t) => this.setState({ availableFrom: t })}
                  value={this.state.availableFrom} /> */}
                {errors.availableFrom && touched.availableFrom && (
                  <div className="invalid-feedback d-block">
                    {errors.availableFrom}
                  </div>
                )}
              </FormGroup>

              <FormGroup className="form-group has-float-label">
                <Label>Available To</Label>
                <Input
                  className="form-control"
                  name="availableTo"
                  required
                  type="time"
                  onChange={handleChange("availableTo")}
                />
                {errors.availableTo && touched.availableTo && (
                  <div className="invalid-feedback d-block">
                    {errors.availableTo}
                  </div>
                )}
              </FormGroup>

              {/* doctorDescription */}
              <FormGroup className="form-group has-float-label">
                <Label>Description</Label>
                <Field
                  className="form-control"
                  name="doctorDescription"
                  required
                  onChange={handleChange("doctorDescription")}
                />
                {errors.doctorDescription && touched.doctorDescription && (
                  <div className="invalid-feedback d-block">
                    {errors.doctorDescription}
                  </div>
                )}
              </FormGroup>

              {/* <FormGroup className="form-group has-float-label">
                 <Label>Status</Label>
                 <Field
                   className="form-control"
                   name="status"
                   checked={this.state.status}
                   type="checkbox"
                   onChange={e => this.handleSatusChange(e.currentTarget.value)}
                  // onChange={handleChange("status")}
                 />
               </FormGroup> */}
              <div className="d-flex justify-content-between align-items-center">
                <Button
                  color="primary"
                  type="submit"
                  className={`btn-shadow btn-multiple-state ${
                    this.props.loading ? "show-spinner" : ""
                    }`}
                  size="lg"
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label"> {isEdit ? "Update " : "Add"}</span>
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (props) => {
  const {
    loading,
    error,
    villages,
    districts,
    genders,
    countries,
    educationList,
    states
  } = props.doctors;
  return { loading, error, villages, districts, genders, countries, educationList, states };
};

export default connect(mapStateToProps, {
  getDoctor,
  addupdateDoctor,
  getVillages,
  getCountry,
  getDistricts,
  getStates,
  getGenders,
  getEducation
})(AddUpdateDoctor);
