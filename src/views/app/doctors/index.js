import React from "react";
import { getDoctors, deleteDoctor, updateDocStatus } from "../../../redux/actions";
import { connect } from "react-redux";
import Table from "react-table";
import { Button, Badge, Modal, ModalHeader, ModalBody, Label, Form } from "reactstrap";
import ReactExport from "react-export-excel";
import AddUpdateDoctor from './add-update-doctors';
import { NotificationManager } from "../../../components/common/react-notifications";
import Switch from "react-switch";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Doctors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          Header: "Doctor ID",
          accessor: "UserID",
          filterable: false,
        },
        {
          Header: "Name",
          accessor: "FullName",
          filterable: false,
        },
        {
          Header: "Contact",
          accessor: "ContactNo",
          filterable: false,
        },
        {
          Header: "Email",
          accessor: "PassMail",
          filterable: false,
        },
        {
          Header: "Father Name",
          accessor: "FatherName",
          filterable: false,
        },
        {
          Header: "Status",
          accessor: "status",
          filterable: false,
          Cell: (record) =>
            record.original.status === true ? (
              <Switch
                offColor="#d6260a"
                onChange={(checked) => this.handleStatusChange(checked, record)}
                checked={record.original.status}
              />
            ) : (
                <Switch
                  offColor="#d6260a"
                  onChange={(checked) => this.handleStatusChange(checked, record)}
                  checked={record.original.status}
                />
              ),
        },
        {
          Header: "Actions",
          sortable: false,
          filterable: false,
          Cell: (record) => (
            <span>
              <Button
                className=""
                color="secondary"
                onClick={() => this.handleEditModel(record)}
              >
                <div title="Edit" className="simple-icon-pencil"></div>
              </Button>
              &nbsp;
              <Button
                className=""
                color="primary"
                onClick={() => this.handleDelModel(record)}
              >
                <div title="Delete" className="simple-icon-trash"></div>
              </Button>
            </span>
          ),
        },
      ],
      page: 0,
      pageSize: 20,
      maxPage: 0,
      total: 0,
      selected: "",
      doctors: [],
      isEdit: false,
      isDel: false,
      isAdd: false,
    };

    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidUpdate(preProps) {
    if (this.props.doctors_data !== preProps.doctors_data) {
      this.setState({
        total: this.props.doctors_data.totalCount,
        maxPage: this.props.doctors_data.maxPage,
        page: this.props.doctors_data.currentPage,
        doctors: this.props.doctors_data.doctors,
      });
    }
  }

  handleEditModel = (record) => {
    this.setState({
      selected: record.original,
      isEdit: !this.state.isEdit,
      isAdd: false,
    });
  };

  handleDelModel = (record) => {
    this.setState({
      selected: record.original,
      isEdit: false,
      isDel: true,
      isAdd: false,
    });
  };

  handleCancel = () => {
    this.setState({ isEdit: false, isDel: false, isAdd: false });
  };

  handleAdd = () => {
    this.setState({ isAdd: !this.state.isAdd, isEdit: false, isDel: false });
  };

  handleDelete() {
    const { selected } = this.state;
    this.props.deleteDoctor(selected.UserID, this.props.history);
    this.setState({ isDel: false });
  };

  handleStatusChange = (checked, record) => {
    let docs = this.state.doctors.map((d) => {
      if (d.UserID == record.original.UserID) {
        d.status = checked;
      }
      return d;
    });
    this.setState(
      {
        selected: record.original,
        doctors: docs,
      },
      () => {
        this.changeDoctorStatus();
      }
    );
  }
  changeDoctorStatus = () => {
    let vals = {
      doctor_id: this.state.selected.UserID,
      status: this.state.selected.status,
    };
    this.props.updateDocStatus(vals, this.props.history);
  }

  filterCaseInsensitive = (filter, row) => {
    const id = filter.pivotId || filter.id;
    return row[id] !== undefined
      ? String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
      : true;
  }

  fetchData = () => {
    // let { date, farmer_name, doctor_name } = this.state;
    // if (data.filtered.length) {
    //   for (let s of data.filtered) {
    //     if (s.id.includes("farmer_name")) {
    //       farmer_name =  s.value.trim();
    //     }
    //     if (s.id.includes("doctor_name")) {
    //       doctor_name =  s.value.trim();
    //     }
    //   }
    //   this.props.getDoctors(
    //     data.page === 0 ? 1 : data.page,
    //     data.pageSize,
    //      date,
    //     farmer_name ,
    //     doctor_name,
    //     this.props.history
    //   );
    // } else {
    this.props.getDoctors(this.state.page || 1, this.state.pageSize, this.props.history);
    // }
  };

  render() {
    const {
      isEdit,
      isDel,
      isAdd,
      columns,
      pageSize,
      maxPage,
      doctors,
    } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
              <h1>Doctors </h1>

              <div className="text-zero top-right-button-container">
                <Button onClick={this.fetchData}> Refresh Status </Button>
                &nbsp;
                <Button onClick={this.handleAdd}> Add Doctor </Button>
                &nbsp;
                <ExcelFile
                  filename="doctors"
                  element={<Button> Download as excel </Button>}
                >
                  <ExcelSheet data={doctors} name="doctors">
                    <ExcelColumn label="Doctor Id" value="UserID" />
                    <ExcelColumn label="Doctor Name" value="FullName" />
                    <ExcelColumn label="Father Name" value="FatherName" />
                    <ExcelColumn label="Email" value="PassMail" />
                    <ExcelColumn label="Contact" value="ContactNo" />
                  </ExcelSheet>
                </ExcelFile>
              </div>
            </div>
          </div>
          <div className="separator mb-5"></div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
              <Table
                manual
                filterable
                showPageSizeOptions={false}
                onFetchData={this.fetchData}
                pages={maxPage}
                defaultFilterMethod={(filter, row) =>
                  this.filterCaseInsensitive(filter, row)
                }
                loading={this.props.loading}
                columns={columns}
                data={doctors}
                defaultPageSize={pageSize}
                className="-striped -highlight"
              />
            </div>
          </div>
        </div>

        <Modal isOpen={isAdd} toggle={this.handleCancel}>
          <ModalHeader toggle={this.handleCancel}>Add Doctor</ModalHeader>
          <ModalBody>
            <AddUpdateDoctor history={this.props.history}/>
          </ModalBody>
        </Modal>

        <Modal isOpen={isEdit} toggle={this.handleCancel}>
          <ModalHeader toggle={this.handleCancel}>Update Doctor</ModalHeader>
          <ModalBody>
            <AddUpdateDoctor doctor={this.state.selected} history={this.props.history}/>
          </ModalBody>
        </Modal>

        <Modal isOpen={isDel} toggle={this.handleCancel}>
          <ModalHeader toggle={this.handleCancel}>
            {this.state.selected ? this.state.selected.FullName : ""}
          </ModalHeader>
          <ModalBody>
            <h4 className="mb-3">Are your sure you want to delete ? </h4>
            <div className="d-flex justify-content-between align-items-left">
              <Button
                color="primary"
                type="submit"
                className={`btn-shadow btn-multiple-state ${
                  this.props.loading ? "show-spinner" : ""
                  }`}
                size="lg"
                onClick={this.handleDelete}
              >
                <span className="spinner d-inline-block">
                  <span className="bounce1" />
                  <span className="bounce2" />
                  <span className="bounce3" />
                </span>
                <span className="label">Delete</span>
              </Button>
            </div>
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}
const mapStateToProps = props => {
  const { success, doctors_data, message, loading, error } = props.doctors;
  return { success, doctors_data, message, loading, error };
};

export default connect(mapStateToProps, {
  getDoctors,
  deleteDoctor,
  updateDocStatus
})(Doctors);
