import React from "react";
import { getSessions } from "../../../redux/actions";
import { connect } from "react-redux";
import Table from "react-table";
import moment from "moment";
import ReactExport from "react-export-excel";
import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file
import { FormGroup, Button, Input } from "reactstrap";
import { DateRangePicker } from "react-date-range";
import { getDateWithFormat } from "./../../../helpers/Utils.js";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Sessions extends React.Component {
  constructor(props) {
    super(props);

    // const now = new Date();
    // const yesterdayBegin = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
    // const todayEnd = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59, 999);
    this.d = new Date();
    this.d.setDate(this.d.getDate() - 30);
    this.state = {
      columns: [
        {
          Header: "Farmer Name",
          accessor: "farmer_name",
        },
        {
          Header: "Farmer Contact",
          accessor: "ContactNo",
        },
        {
          Header: "Doctor Name",
          accessor: "doctor_name",
        },
        {
          Header: "Village",
          accessor: "VillageName",
        },
        {
          Header: "District",
          accessor: "DistrictName",
        },
        {
          Header: "VLE Name",
          accessor: "vle_name",
        },
        {
          Header: "VLE Contact",
          accessor: "vle_contact",
        },
        {
          Header: "Session Time Limit",
          accessor: "session_time_limit",
          sortable: false,
        },
        {
          Header: "Start Date",
          accessor: "start_time",
          resizable: true,
          minWidth: 160,
          // sortable: false,
          Cell: (record) =>
            moment.utc(record.original.start_time).format("LL , hh:mm:a"),
        },
      ],
      page: 1,
      pageSize: 50,
      maxPage: 0,
      farmer_name: "",
      doctor_name: "",
      village_name: "",
      farmer_contact: "",
      vle_name: "",
      total: 0,
      selected: {},
      sessions: [],
      date_value: [this.d, new Date()],
      showDateRangeSelect: false,
      pageNumber: 0,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  static getDerivedStateFromProps(props, state) {
    return {
      total: props.session_data.totalCount,
      maxPage: props.session_data.maxPage,
      page: props.session_data.currentPage,
      sessions: props.session_data.sessions,
    };
  }

  handleSearch = (e) => {
    e.preventDefault();
    let {
      page,
      pageSize,
      date,
      farmer_name,
      doctor_name,
      farmer_contact,
      village_name,
      vle_name,
      date_value,
    } = this.state;
    let data = {
      page: 1,
      page_size: Number.MAX_SAFE_INTEGER,
      date,
      farmer_name,
      doctor_name,
      from_date: date_value ? getDateWithFormat(date_value[0]) : "",
      to_date: date_value ? getDateWithFormat(date_value[1]) : "",
      user_id: this.props.user,
      vle_name,
      farmer_contact,
    };
    this.props.getSessions(data, this.props.history);
  };

  handleDateSelect = ({ selection }) => {
    this.setState({
      date_value: [selection.startDate, selection.endDate],
    });
  };

  closeDateRangePicker = () => {
    this.setState({ showDateRangeSelect: false });
  };

  updatePageNumber = (page) => {
    this.setState({ pageNumber: page });
  };

  getSessionsForTable = () => {
    return this.state.sessions.slice(
      this.state.pageNumber * 50,
      this.state.pageNumber * 50 + 50
    );
  };

  filterCaseInsensitive = (filter, row) => {
    const id = filter.pivotId || filter.id;
    return row[id] !== undefined
      ? String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
      : true;
  };

  fetchData = () => {
    let {
      date,
      farmer_name,
      doctor_name,
      farmer_contact,
      village_name,
      vle_name,
      page,
      pageSize,
      date_value,
    } = this.state;
    this.props.getSessions(
      {
        page: 1,
        page_size: Number.MAX_SAFE_INTEGER,
        date,
        farmer_name,
        doctor_name,
        from_date: date_value ? getDateWithFormat(date_value[0]) : "",
        to_date: date_value ? getDateWithFormat(date_value[1]) : "",
        user_id: this.props.user,
        vle_name,
        farmer_contact,
      },
      this.props.history
    );
  };

  handleChange = (e, name) => {
    this.setState({ [name]: e.target.value });
  };

  render() {
    const { columns, pageSize, maxPage, sessions } = this.state;
    const selectionRange = {
      startDate: this.state.date_value[0],
      endDate: this.state.date_value[1],
      key: "selection",
    };

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
              <h1>Sessions </h1>
              <div className="text-zero top-right-button-container">
                <ExcelFile
                  filename="sessions"
                  element={<Button> Download as excel </Button>}
                >
                  <ExcelSheet data={sessions} name="sessions">
                    <ExcelColumn label="Farmer" value="farmer_name" />
                    <ExcelColumn label="Farmer Contact" value="ContactNo" />
                    <ExcelColumn label="Doctor" value="doctor_name" />
                    <ExcelColumn label="Village" value="VillageName" />
                    <ExcelColumn label="District " value="DistrictName" />
                    <ExcelColumn label="VLEName " value="vle_name" />
                    <ExcelColumn label="VLE Contact " value="vle_contact" />
                    <ExcelColumn label="Start Date " value="start_time" />
                  </ExcelSheet>
                </ExcelFile>
              </div>
            </div>
          </div>
          <div className="separator mb-5"></div>
        </div>
        <div className="row">
          <div className="col-12">
            <form
              onSubmit={this.handleSearch}
              className="av-tooltip tooltip-label-bottom"
            >
              <FormGroup className="form-group custom-input">
                <Input
                  className="form-control"
                  name="farmer_name"
                  placeholder="Farmer Name"
                  onChange={(e) => this.handleChange(e, "farmer_name")}
                />
              </FormGroup>
              <FormGroup className="form-group custom-input">
                <Input
                  className="form-control"
                  name="doctor_name"
                  placeholder="Doctor Name"
                  onChange={(e) => this.handleChange(e, "doctor_name")}
                />
              </FormGroup>
              <FormGroup className="form-group custom-input">
                <Input
                  className="form-control"
                  name="vle_name"
                  placeholder="VLE Name"
                  onChange={(e) => this.handleChange(e, "vle_name")}
                />
              </FormGroup>
              <FormGroup className="form-group custom-input">
                <Input
                  className="form-control"
                  name="village_name"
                  placeholder="Village Name"
                  onChange={(e) => this.handleChange(e, "village_name")}
                />
              </FormGroup>
              <FormGroup className="form-group custom-input">
                <Input
                  className="form-control"
                  name="farmer_contact"
                  placeholder="Farmer Contact"
                  onChange={(e) => this.handleChange(e, "farmer_contact")}
                />
              </FormGroup>

              <FormGroup className="form-group custom-input">
                <div className="d-flex justify-content-between align-items-center">
                  <Button
                    color="secondary"
                    className={`btn-shadow btn-multiple-state`}
                    size="lg"
                    onClick={() =>
                      this.setState({
                        showDateRangeSelect: !this.state.showDateRangeSelect,
                      })
                    }
                  >
                    {`${getDateWithFormat(
                      selectionRange.startDate
                    )} - ${getDateWithFormat(selectionRange.endDate)}`}
                  </Button>
                  <Button
                    color="secondary"
                    className={`btn-shadow btn-multiple-state`}
                    onClick={() =>
                      this.setState({
                        date_value: [this.d, new Date()],
                      })
                    }
                  >
                    X
                  </Button>
                </div>
              </FormGroup>
              {this.state.showDateRangeSelect && (
                <FormGroup className="form-group custom-input show-ranger">
                  <DateRangePicker
                    ranges={[selectionRange]}
                    onChange={this.handleDateSelect}
                    scroll={{ enabled: true }}
                    maxDate={new Date()}
                  />
                </FormGroup>
              )}
              <div className="d-flex justify-content-between align-items-center">
                <Button
                  color="secondary"
                  type="submit"
                  className={`btn-shadow btn-multiple-state`}
                  size="lg"
                  onClick={this.closeDateRangePicker}
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label">Search</span>
                </Button>
              </div>
            </form>
          </div>
          <div className="col-12">
            <div className="mb-2" onClick={this.closeDateRangePicker}>
              <Table
                id="session"
                manual
                showPageSizeOptions={false}
                onPageChange={(page) => this.updatePageNumber(page)}
                // onFetchData={(state) => this.fetchData(state)}
                pages={Math.ceil(sessions.length / 50)}
                defaultFilterMethod={(filter, row) =>
                  this.filterCaseInsensitive(filter, row)
                }
                columns={columns}
                data={this.getSessionsForTable()}
                loading={this.props.loading}
                // defaultPageSize={50}
                pageSize={50}
                className="-striped -highlight"
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  const { success, session_data, message, loading, error } = state.sessions;
  const { user } = state.authUser;
  return { success, session_data, message, loading, error, user };
};

export default connect(mapStateToProps, {
  getSessions,
})(Sessions);
