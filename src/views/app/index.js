import React, { Component, Suspense } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import AppLayout from "../../layout/AppLayout";

const Gogo = React.lazy(() =>
  import(/* webpackChunkName: "viwes-gogo" */ "./gogo")
);
const SecondMenu = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ "./second-menu")
);
const BlankPage = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ "./blank-page")
);
const Dashboard = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ "./dashboard")
);

const Sessions = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ "./sessions")
);
const Doctors = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ "./doctors")
);
const DoctorsMapping = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ "./doctor-mapping")
);

const Settings = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ "./settings")
);




class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <AppLayout>
        <div className="dashboard-wrapper">
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Redirect
                exact
                from={`${match.url}/`}
                to={`${match.url}/sessions`}
              />
              <Route
                path={`${match.url}/dashboard`}
                render={props => <Dashboard {...props} />}
              />
              <Route
                path={`${match.url}/sessions`}
                render={props => <Sessions {...props} />}
              />
               <Route
                path={`${match.url}/doctors`}
                render={props => <Doctors {...props} />}
              />
               <Route
                path={`${match.url}/doctors-mapping`}
                render={props => <DoctorsMapping {...props} />}
              />
              <Route
                path={`${match.url}/settings`}
                render={props => <Settings {...props} />}
              />
              
              <Route
                path={`${match.url}/gogo`}
                render={props => <Gogo {...props} />}
              />
              <Route
                path={`${match.url}/second-menu`}
                render={props => <SecondMenu {...props} />}
              />
              <Route
                path={`${match.url}/blank-page`}
                render={props => <BlankPage {...props} />}
              />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
