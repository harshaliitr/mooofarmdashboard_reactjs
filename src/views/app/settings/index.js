import React from "react";
import { getSettings, deleteSettings } from "../../../redux/actions";
import { connect } from "react-redux";
import Table from "react-table";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  FormGroup,
} from "reactstrap";
import ReactExport from "react-export-excel";
import UpdateSetting from './update-setting';
import moment from "moment";
import { NotificationManager } from "../../../components/common/react-notifications";


const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          Header: "Setting Name",
          accessor: "SettingName",
          filterable: false,
        },
        {
          Header: "Setting Value",
          accessor: "SettingValue",
          filterable: false,
        },
        {
          Header: "Setting Description",
          accessor: "SettingDescription",
          filterable: false,
        },
        {
            Header: "Date",
            accessor: "timestamp",
            // sortable: false,
            filterable: false,
            Cell: record => (
              moment.utc(record.original.timestamp).format('LL , hh:mm:a') 
             ),
         },
        {
          Header: "Actions",
          sortable: false,
          filterable: false,
          Cell: (record) => (
            <span>
              <Button
                className=""
                color="secondary"
                onClick={() => this.handleEditModel(record)}
              >
                <div title="Edit" className="simple-icon-pencil"></div>
              </Button>
              &nbsp;
            </span>
          ),
        },
      ],
      page: 0,
      pageSize: 10,
      maxPage: 0,
      total: 0,
      selected: '',
      settings: [],
      isEdit: false,
      isDel: false,
      isAdd: false
    };
  }

  
  handleEditModel = record => {
    this.setState({
      selected: record.original,
      isEdit: !this.state.isEdit,
      isAdd: false
    });
  };

  handleCancel = () => {
    this.setState({ isEdit: false, isDel: false, isAdd: false });
  };

  handleAdd = () => {
    this.setState({ isAdd: !this.state.isAdd, isEdit: false, isDel: false });
  };


  hideAdd = () => {
    this.setState({ isAdd: false, isEdit: false, isDel: false });
    let { page, pageSize } = this.state;
    this.props.getSettings(page, pageSize, this.props.history);
  };


  componentDidUpdate(preProps) {
    if (this.props.settings !== preProps.settings) {
      this.setState({
        total: this.props.settings.totalCount,
        maxPage: this.props.settings.maxPage,
        page: this.props.settings.currentPage,
        settings: this.props.settings.settings,
      });
    }
  
  }

  filterCaseInsensitive(filter, row) {
    const id = filter.pivotId || filter.id;
    return row[id] !== undefined
      ? String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
      : true;
  }

  fetchData = (data) => {
    // let { date, farmer_name, doctor_name } = this.state;
    // if (data.filtered.length) {
    //   for (let s of data.filtered) {
    //     if (s.id.includes("farmer_name")) {
    //       farmer_name =  s.value.trim();
    //     }
    //     if (s.id.includes("doctor_name")) {
    //       doctor_name =  s.value.trim();
    //     }
    //   }
    //   this.props.getDoctors(
    //     data.page === 0 ? 1 : data.page,
    //     data.pageSize,
    //      date,
    //     farmer_name ,
    //     doctor_name,
    //     this.props.history
    //   );
    // } else {
    this.props.getSettings(data.page + 1, data.pageSize, this.props.history);
    // }
  };

  render() {
    const { isEdit, isDel, isAdd, columns, pageSize, maxPage, settings } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
              <h1>Settings </h1>
             
              <div className="text-zero top-right-button-container">
                <ExcelFile
                  filename="settings"
                  element={<Button> Download as excel </Button>}
                >
                  <ExcelSheet data={settings} name="settings">
                    <ExcelColumn label="Name" value="SettingName" />
                    <ExcelColumn label="Value" value="SettingValue" />
                    <ExcelColumn label="Description" value="SettingDescription" />
                    <ExcelColumn label="Date" value="timestamp" />
                  </ExcelSheet>
                </ExcelFile>
              </div>
            </div>
          </div>
          <div className="separator mb-5"></div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="mb-2">
                <Table
                  manual
                  filterable
                  showPageSizeOptions={false}
                  onFetchData={(state) => this.fetchData(state)}
                  pages={maxPage}
                  defaultFilterMethod={(filter, row) =>
                    this.filterCaseInsensitive(filter, row)
                  }
                  columns={columns}
                  data={settings}
                  defaultPageSize={pageSize}
                  className="-striped -highlight"
                  loading={this.props.loading}
                />
            </div>
          </div>
        </div>

        <Modal isOpen={isEdit} toggle={this.handleCancel}>
          <ModalHeader toggle={this.handleCancel}>Update Setting</ModalHeader>
          <ModalBody>
            <UpdateSetting setting={this.state.selected} hideAdd={this.hideAdd} />
          </ModalBody>
        </Modal>

      </React.Fragment>
    );
  }
}
const mapStateToProps = props => {
  const { success, settings, message, loading, error } = props.settings;
  return { success, settings, message,  loading, error };
};

export default connect(mapStateToProps, {
  getSettings,
  deleteSettings,
})(Settings);
