import React from "react";
import { updateSettings } from "../../../redux/actions";
import { connect } from "react-redux";
import { Formik, Form, Field } from "formik";
import { Label, FormGroup, Button, Input } from "reactstrap";

class UpdateSetting extends React.Component {
  constructor(props) {
    super(props);
    const { setting } = this.props;
    this.state = {
      isEdit: setting ? true : false,
      settingName: setting ? setting.SettingName : "",
      settingValue: setting ? setting.SettingValue : "",
    };
  }

  static getDerivedStateFromProps(props, state) {
    return {
      settingName: props.setting.SettingName,
      settingValue: props.setting.SettingValue
    }
  }

  addSetting = (values) => {
    const { setting } = this.props;
    let settingId = setting ? setting.SettingID : "";
    if (!this.props.loading) {
      let vals = {
        settingId,
        settingName: values.settingName,
        settingValue: values.settingValue,
      };
      this.props.updateSettings(vals, this.props.history);
      this.props.hideAdd()
    }
  };

  render() {
    const { isEdit } = this.state;
    return (
      <React.Fragment>
        <Formik initialValues={this.state} onSubmit={this.addSetting}>
          {({ errors, handleChange, touched }) => (
            <Form className="av-tooltip tooltip-label-bottom">
              <FormGroup className="form-group has-float-label">
                <Label>Setting Name</Label>
                <Field
                  className="form-control"
                  name="settingName"
                  required
                  onChange={() => {}}
                  value={this.state.settingName}
                />
                {errors.settingName && touched.settingName && (
                  <div className="invalid-feedback d-block">
                    {errors.settingName}
                  </div>
                )}
              </FormGroup>
              <FormGroup className="form-group has-float-label">
                <Label>Setting's Value</Label>
                <Field
                  className="form-control"
                  name="settingValue"
                  required
                  onChange={handleChange("settingValue")}
                />
                {errors.settingValue && touched.settingValue && (
                  <div className="invalid-feedback d-block">
                    {errors.settingValue}
                  </div>
                )}
              </FormGroup>
              <div className="d-flex justify-content-between align-items-center">
                <Button
                  color="primary"
                  type="submit"
                  className={`btn-shadow btn-multiple-state ${
                    this.props.loading ? "show-spinner" : ""
                  }`}
                  size="lg"
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label"> {isEdit ? "Update " : "Add"}</span>
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (props) => {
  const { loading, error } = props.settings;
  return { loading, error };
};

export default connect(mapStateToProps, {
  updateSettings,
})(UpdateSetting);
